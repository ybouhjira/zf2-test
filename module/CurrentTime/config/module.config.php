<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'CurrentTime\Controller\Index' => 'CurrentTime\Controller\IndexController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'time' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/time',
                    'defaults' => array(
                        '__NAMESPACE__' => 'CurrentTime\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'users' => __DIR__ . '/../view',
        ),
    ),
);