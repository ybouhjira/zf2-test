<?php

namespace CurrentTime\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        // return new ViewModel();
        $view = new ViewModel();
        $view->setTemplate('current-time/index/index');
        $view->setVariable('time', date('h:m:s'));
        $view->setVariable('date', date('j-M-Y'));
        return $view;
    }
}

